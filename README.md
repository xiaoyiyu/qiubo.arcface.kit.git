# 虹软人脸识别IOS库

> 虹软视觉开放平台提供以免费的人脸识别SDK技术，为开发者免费提供人脸识别、人脸检测、人脸追踪、活体检测、人证比对技术等SDK。提供智能解决方案，致力于为创业团队提供视觉智能技术支撑。

+ [官网](https://ai.arcsoft.com.cn/index.html)
+ [文档](https://ai.arcsoft.com.cn/manual/docs#/107)